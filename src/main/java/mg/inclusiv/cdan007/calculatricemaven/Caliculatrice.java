/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan007.calculatricemaven;

/**
 *
 * @author djibo
 */
public class Caliculatrice {
   public int addition (int nombre1 ,int nombre2) {
        return nombre1 + nombre2;
    }
    public int soustraction (int nombre1 ,int nombre2) {
        return nombre1 - nombre2;
    }

    public float division(float dividende ,float diviseur) {
        if (diviseur != 0) {
            return dividende / diviseur;
        } else {
            System.err.println("Division par zéro");
        }
        return 0 ;
    }

    public float calculmoyenne(float[] nombres){
        float total = 0;
            for (float nombre : nombres){
                total += nombre;
            }

         return division(total, nombres.length);
    } 
}
